import {Component, OnInit} from '@angular/core';
import {ReservaDto} from '../../interfaces/reservaDto';
import {ReservaService} from '../../services/reserva.service';

@Component({
    selector: 'app-tab4',
    templateUrl: './tab4.page.html',
    styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

    reservas: ReservaDto[];

    constructor(private reservaService: ReservaService) {
    }

    ngOnInit() {
        this.getReservas();
    }

    getReservas() {
        this.reservaService.getReservas().subscribe(resp => {
            this.reservas = resp.data;
        });
    }

    doRefresh(event) {
        this.getReservas();
        event.target.complete();
    }
}
